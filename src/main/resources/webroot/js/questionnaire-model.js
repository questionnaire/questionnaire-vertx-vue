var baseUrl = "http://localhost:8080"
var app = new Vue({
  el: '#app',
  data: {
  // list of json questionnaires
    questionnaires: [],
    // selected json questionnaire
    selected: null
  }
})

var eventHub = new Vue()

if(window.localStorage.getItem("questionnaires") !=null){
    var questionnaires = JSON.parse(window.localStorage.getItem("questionnaires"));
    Array.prototype.push.apply(app.questionnaires, questionnaires);
    app.selected=JSON.parse(window.localStorage.getItem("selected"));
    eventHub.$emit('questionnaireSelected', app.selected.id);
    synchronize();
}


document.getElementById("questionnairesList").addEventListener("click", function(e){
    console.log("Hi man from "+e.target)
    var isListItem = e.target.matches(".questionnaireListItem");
    var isSelected = e.target.matches(".selected")
    if(e.target && isListItem && !isSelected){
        getQuestionnaire(e.target.getAttribute("questionnaireId"))
        eventHub.$emit('questionnaireSelected', e.target.getAttribute("questionnaireId"));
    }
})

function storeState(){
    window.localStorage.setItem("questionnaires", JSON.stringify(app.questionnaires));
    window.localStorage.setItem("selected", JSON.stringify(app.selected));
}

function synchronize() {
    var xhr = new XMLHttpRequest();
    var data = {"questionnaires":JSON.parse(window.localStorage.getItem("questionnaires"))}
    xhr.open("PUT", baseUrl+"/synchronize", true);
    xhr.responseType="json"
    xhr.setRequestHeader("Content-Type", "text/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            console.log("TODO : sync");
        } else {
            console.log("issue while syncing "+xhr.readyState+" "+xhr.status)
        }
    };
    xhr.send(JSON.stringify(data));
}

function createQuestionnaire() {
    var xhr = new XMLHttpRequest();
    var url = "url";
    var data = {"title":document.getElementById("title").value}
    xhr.open("PUT", baseUrl+"/questionnaires", true);
    xhr.responseType="json"
    xhr.setRequestHeader("Content-Type", "text/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var json = xhr.response;
            app.questionnaires.push(json)
            app.selected=json
            storeState()
            console.log("questionnaire created");
            document.getElementById("title").value=''
        } else {
            console.log("issue while creating questionnaire "+xhr.readyState+" "+xhr.status)
        }
    };
    xhr.send(JSON.stringify(data));
}

function getQuestionnaire(id) {
    var xhr = new XMLHttpRequest();
    var url = "url";
    xhr.open("GET", baseUrl+"/questionnaires/"+id, true);
    xhr.responseType="json"
    xhr.setRequestHeader("Content-Type", "text/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var json = xhr.response;
            app.selected=json
            storeState()
            console.log("questionnaire got");
        } else {
            console.log("issue while getting questionnaire "+id+" "+xhr.readyState+" "+xhr.status)
        }
    };
    xhr.send();
}
function createQuestion() {
    var xhr = new XMLHttpRequest();
    var url = "url";
    var data = {
        "wording":document.getElementById("wording").value,
        "nbLines":document.getElementById("nbLines").value
        }
    var questionnaireID = document.getElementById("selectedQuestionnaireId").textContent;
    xhr.open("PUT", baseUrl+"/questionnaires/"+questionnaireID+"/questions", true);
    xhr.responseType="json"
    xhr.setRequestHeader("Content-Type", "text/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var json = xhr.response;
            app.selected.questions.push(json)
            storeState()
            console.log("question created");
            document.getElementById("wording").value=''
            document.getElementById("nbLines").value=''
        } else {
            console.log("issue while creating questionnaire "+xhr.readyState+" "+xhr.status)
        }
    };
    xhr.send(JSON.stringify(data));
}