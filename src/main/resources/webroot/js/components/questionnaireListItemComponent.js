Vue.component('questionnaire-listitem', {
    props:['id','title','selected'],
    data: function(){
        return {
            selectedQuestionnaire :this.selected
        }
    },
    template:`
        <li>
            <span v-bind:class="{'selectedQuestionnaire':(selectedQuestionnaire)}"
                class="questionnaireListItem" v-bind:questionnaireId="id" v-bind:title="id" >
                {{title}}
            </span>
        </li>
    `,
    created: function () {
      eventHub.$on('questionnaireSelected', this.doSelect)
    },
    methods:{
        doSelect:function(questionnaireId){
            console.log("trying to be selected " + questionnaireId)
            this.selectedQuestionnaire=(questionnaireId===this.id)
        }
    }
})