package org.egedede.questionnaire.export.pdf;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.egedede.questionnaire.model.Questionnaire;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

/**
 *
 */
public class PdfExporter {

    public byte[] export(Questionnaire questionnaire){
        PDDocument doc = new PDDocument();
        byte[] result = new byte[0];
        try{
            PDPage page = new PDPage(PDRectangle.A4);
            doc.addPage(page);
            PDRectangle mediabox = page.getMediaBox();
            float margin = 72;
            float width = mediabox.getWidth() - 2*margin;
            float startX = mediabox.getLowerLeftX() + margin;
            float startY = mediabox.getUpperRightY() - margin;
            float height = mediabox.getHeight() - 2*margin;
            float x= startX;
            float y = startY;
            PDFont font = PDType1Font.HELVETICA_BOLD;
            PDPageContentStream contents = null;
            contents = new PDPageContentStream(doc, page);
            contents.beginText();
            float fontSize = 12;
            float leading = 1.5f*fontSize;
            contents.setFont(font, fontSize);
            contents.newLineAtOffset(startX, startY);
            contents.showText("p ");
            contents.newLineAtOffset(10, -leading);
            x+=10;
            y-=leading;
            contents.showText(questionnaire.getId());
            contents.newLineAtOffset(0, -leading);
            x+=0;
            y-=leading;
            contents.showText("New line "+x+","+y);
            contents.newLineAtOffset(10, -leading);
            x+=10;
            y-=leading;
            contents.showText("Oh yeah "+x+","+y);
            x-=20;
            y-=leading;
            contents.endText();
            PDImageXObject image = drawImage(doc);
            contents.drawImage(image,x,y-image.getHeight());
            y-=image.getHeight()+leading;
            contents.beginText();
            contents.newLineAtOffset(x, y);
            contents.showText("Hi there, do you see me? "+x+", "+(y-image.getHeight()-leading));
            contents.endText();
            contents.close();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            doc.save(baos);
            result = baos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            try {
                doc.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
            return result;

    }

    public PDImageXObject drawImage(PDDocument document) throws Exception {
        byte[] baos = Files.readAllBytes(Paths.get("/home/pstruve/Pictures","messageCompteLatentCreation.png"));
        String s = Base64.getEncoder().encodeToString(baos);
        BufferedImage buffer = ImageIO.read(new ByteArrayInputStream(Base64.getDecoder().decode(s)));
        PDImageXObject image  = LosslessFactory.createFromImage(document, buffer);
        return image;
    }
    public float getStringWidth(String content, PDFont font, float fontSize) throws Exception{
        return fontSize * font.getStringWidth(content) / 1000;
    }
}
