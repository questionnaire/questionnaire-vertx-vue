package org.egedede.questionnaire;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.egedede.questionnaire.utils.PATCH;

import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Base64;

/**
 *
 */
@Path("/")
public class Controller {

    @GET
    @Path("")
    @Produces({MediaType.APPLICATION_JSON})
    public void index(@Suspended final AsyncResponse asyncResponse){
        asyncResponse.resume("Plop");
    }

    @GET
    @Path("/questionnaires/{questionnaireID}")
    @Produces({MediaType.APPLICATION_JSON})
    public void get(

            // Suspend the request
            @Suspended final AsyncResponse asyncResponse,

            // Inject the Vertx instance
            @Context Vertx vertx,

            @PathParam("questionnaireID") String questionnaireID
    ) {

        if (questionnaireID == null) {
            asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
            return;
        }

        // Send a get message to the backend
        vertx.eventBus().<JsonObject>send("backend", new JsonObject()
                .put("op", "get")
                .put("id", questionnaireID), msg -> {

            // When we get the response we resume the Jax-RS async response
            if (msg.succeeded()) {
                JsonObject json = msg.result().body();
                if (json != null) {
                    asyncResponse.resume(json.encode());
                } else {
                    asyncResponse.resume(Response.status(Response.Status.NOT_FOUND).build());
                }
            } else {
                asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR).build());
            }
        });
    }

    @GET
    @Path("/questionnaires/{questionnaireID}/export")
    @Produces({MediaType.APPLICATION_OCTET_STREAM})
    public void export(

            // Suspend the request
            @Suspended final AsyncResponse asyncResponse,

            // Inject the Vertx instance
            @Context Vertx vertx,

            @PathParam("questionnaireID") String questionnaireID
    ) {
        System.out.println("Controller.export "+questionnaireID);
        if (questionnaireID == null) {
            asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
            return;
        }
        System.out.println("Controller.export 2 "+questionnaireID);

        // Send a get message to the backend
        vertx.eventBus().<String>send("backend", new JsonObject()
                .put("op", "export")
                .put("id", questionnaireID), msg -> {

            // When we get the response we resume the Jax-RS async response
            if (msg.succeeded()) {
                String json = msg.result().body();
                System.out.println("Controller.export "+json);
                if (json != null) {
                    Response response = Response.status(Response.Status.OK)
                            .header("content-disposition", "attachment;filename=\""+questionnaireID + ".pdf\"")
                            .header("content-type", "application/pdf")
                            .entity(Base64.getDecoder().decode(json))
                            .build();
                    asyncResponse.resume(response);
                } else {
                    asyncResponse.resume(Response.status(Response.Status.NOT_FOUND).build());
                }
            } else {
                asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR).build());
            }
        });
    }

    @PUT
    @Path("/synchronize")
    @Produces({MediaType.APPLICATION_JSON})
    public void synchronize(

            // Suspend the request
            @Suspended final AsyncResponse asyncResponse,

            // Inject the Vertx instance
            @Context Vertx vertx,

            String dataAsText
    ) {
        System.out.println("Controller.synchronize "+dataAsText);
        if (dataAsText == null) {
            asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
            return;
        }

        JsonObject data;
        try {
            data = new JsonObject(dataAsText);
        } catch (Exception e) {
            e.printStackTrace();
            asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
            return;
        }

        // Send an add message to the backend
        vertx.eventBus().<JsonArray>send("backend", new JsonObject()
                .put("op", "synchronize")
                .put("questionnaires", data.getJsonArray("questionnaires")), msg -> {

            // When we get the response we resume the Jax-RS async response
            System.out.println("Controller.synchronize "+msg.succeeded());
            System.out.println("Controller.synchronize "+msg.result());
            System.out.println("Controller.synchronize "+msg.result());
            if (msg.succeeded()) {
                if (msg.result().body().size()>0) {
                    asyncResponse.resume(
                            Response
                                    .status(Response.Status.OK)
                                    .entity(msg.result().body())
                                    .build());
                } else {
                    asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
                }
            } else {
                asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR).build());
            }
        });
    }

    @PUT
    @Path("/questionnaires")
    @Produces({MediaType.APPLICATION_JSON})
    public void putQuestionnaire(

            // Suspend the request
            @Suspended final AsyncResponse asyncResponse,

            // Inject the Vertx instance
            @Context Vertx vertx,

            String dataAsText
    ) {
        System.out.println("Controller.putQuestionnaire "+dataAsText);
        if (dataAsText == null) {
            asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
            return;
        }

        JsonObject data;
        try {
            data = new JsonObject(dataAsText);
        } catch (Exception e) {
            e.printStackTrace();
            asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
            return;
        }

        // Send an add message to the backend
        vertx.eventBus().<String>send("backend", new JsonObject()
                .put("op", "add")
                .put("title", data.getString("title")), msg -> {

            // When we get the response we resume the Jax-RS async response
            System.out.println("Controller.put "+msg.succeeded());
            System.out.println("Controller.put "+msg.result());
            if (msg.succeeded()) {
                if (msg.result().body().length()>0) {
                    asyncResponse.resume(
                            Response
                                    .status(Response.Status.OK)
                                    .entity(msg.result().body())
                                    .build());
                } else {
                    asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
                }
            } else {
                asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR).build());
            }
        });
    }

    @PUT
    @Path("/questionnaires/{questionnaireID}/questions")
    @Produces({MediaType.APPLICATION_JSON})
    public void putQuestion(

            // Suspend the request
            @Suspended final AsyncResponse asyncResponse,

            // Inject the Vertx instance
            @Context Vertx vertx,

            @PathParam("questionnaireID") String questionnaireID,
            String dataAsText
    ) {

        if (questionnaireID == null) {
            asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
            return;
        }

        JsonObject questionUpdated = new JsonObject(dataAsText);

        // Send an add message to the backend
        vertx.eventBus().<String>send("backend", new JsonObject()
                .put("op", "addQuestion")
                .put("id", questionnaireID)
                .put("wording", questionUpdated.getString("wording"))
                .put("nbLines", Integer.parseInt(questionUpdated.getString("nbLines"))), msg -> {

            // When we get the response we resume the Jax-RS async response
            if (msg.succeeded()) {
                if (msg.result().body().length()>0) {
                    asyncResponse.resume(Response
                            .status(Response.Status.OK)
                            .entity(msg.result().body())
                            .build());
                } else {
                    asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
                }
            } else {
                asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR).build());
            }
        });
    }

    @GET
    @Path("/questionnaires")
    @Produces({MediaType.APPLICATION_JSON})
    public void list(

            // Suspend the request
            @Suspended final AsyncResponse asyncResponse,

            // Inject the Vertx instance
            @Context Vertx vertx) {
        System.out.println("Trying to get questionnaires");
        // Send a list message to the backend
        vertx.eventBus().<JsonArray>send("backend", new JsonObject().put("op", "list"), msg -> {

            // When we get the response we resume the Jax-RS async response
            if (msg.succeeded()) {
                JsonArray json = msg.result().body();
                if (json != null) {
                    asyncResponse.resume(json.encode());
                } else {
                    asyncResponse.resume(Response.status(Response.Status.NOT_FOUND).build());
                }
            } else {
                System.err.println(msg.cause());
                asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR).build());
            }
        });
    }

    /**
     * Aim here is to do some stuff between two items.
     * Typically : question1 on top of question2 or question1 on bottom of question2
     * @param asyncResponse
     * @param vertx
     * @param questionnaireID
     * @param action
     * @param question1
     * @param question2
     */
    @PATCH
    @Path("questionnaires/{questionnaireID}/{action}/{question1}/{question2}")
    @Produces({MediaType.APPLICATION_JSON})
    public void patch(// Suspend the request
                   @Suspended final AsyncResponse asyncResponse,

                   // Inject the Vertx instance
                   @Context Vertx vertx,
                      @PathParam("questionnaireID") String questionnaireID,
                      @PathParam("action") String action,
                      @PathParam("question1ID") String question1,
                      @PathParam("question2ID") String question2
                      ){
        if (questionnaireID == null) {
            asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
            return;
        }

        JsonObject productJson;
        try {
            productJson = new JsonObject();
        } catch (Exception e) {
            asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
            return;
        }

        // Send an add message to the backend
        vertx.eventBus().<Boolean>send("backend", new JsonObject()
                .put("op", "add")
                .put("id", questionnaireID)
                .put("product", productJson), msg -> {

            // When we get the response we resume the Jax-RS async response
            if (msg.succeeded()) {
                if (msg.result().body()) {
                    asyncResponse.resume(Response.status(Response.Status.OK).build());
                } else {
                    asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).build());
                }
            } else {
                asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR).build());
            }
        });
    }
}
