package org.egedede.questionnaire.utils;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//



import io.netty.buffer.ByteBufInputStream;
import io.vertx.core.Context;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import java.io.IOException;

import io.vertx.ext.web.RoutingContext;
import org.jboss.resteasy.core.SynchronousDispatcher;
import org.jboss.resteasy.plugins.server.embedded.SecurityDomain;
import org.jboss.resteasy.plugins.server.vertx.RequestDispatcher;
import org.jboss.resteasy.plugins.server.vertx.VertxHttpRequest;
import org.jboss.resteasy.plugins.server.vertx.VertxHttpResponse;
import org.jboss.resteasy.plugins.server.vertx.VertxUtil;
import org.jboss.resteasy.plugins.server.vertx.i18n.LogMessages;
import org.jboss.resteasy.plugins.server.vertx.i18n.Messages;
import org.jboss.resteasy.specimpl.ResteasyHttpHeaders;
import org.jboss.resteasy.spi.Failure;
import org.jboss.resteasy.spi.ResteasyDeployment;
import org.jboss.resteasy.spi.ResteasyUriInfo;

public class VRHandler implements Handler<RoutingContext> {
    private final Vertx vertx;
    protected final RequestDispatcher dispatcher;
    private final String servletMappingPrefix;

    public VRHandler(Vertx vertx, ResteasyDeployment deployment, String servletMappingPrefix, SecurityDomain domain) {
        this.vertx = vertx;
        this.dispatcher = new RequestDispatcher((SynchronousDispatcher)deployment.getDispatcher(), deployment.getProviderFactory(), domain);
        this.servletMappingPrefix = servletMappingPrefix;
    }

    public VRHandler(Vertx vertx, ResteasyDeployment deployment, String servletMappingPrefix) {
        this(vertx, deployment, servletMappingPrefix, (SecurityDomain)null);
    }

    public VRHandler(Vertx vertx, ResteasyDeployment deployment) {
        this(vertx, deployment, "");
    }

    public void handle(RoutingContext context) {
        HttpServerRequest request = context.request();
        request.bodyHandler((buff) -> {
            Context ctx = this.vertx.getOrCreateContext();
            ResteasyUriInfo uriInfo = VertxUtil.extractUriInfo(request, this.servletMappingPrefix);
            ResteasyHttpHeaders headers = VertxUtil.extractHttpHeaders(request);
            HttpServerResponse response = request.response();
            VertxHttpResponse vertxResponse = new VertxHttpResponse(response, this.dispatcher.getProviderFactory());
            VertxHttpRequest vertxRequest = new VertxHttpRequest(ctx, headers, uriInfo, request.rawMethod(), this.dispatcher.getDispatcher(), vertxResponse, false);
            if (buff.length() > 0) {
                ByteBufInputStream in = new ByteBufInputStream(buff.getByteBuf());
                vertxRequest.setInputStream(in);
            }

            try {
                this.dispatcher.service(ctx, request, response, vertxRequest, vertxResponse, true);
            } catch (Failure var11) {
                vertxResponse.setStatus(var11.getErrorCode());
            } catch (Exception var12) {
                vertxResponse.setStatus(500);
                LogMessages.LOGGER.error(Messages.MESSAGES.unexpected(), var12);
            }

            if (!vertxRequest.getAsyncContext().isSuspended()) {
                try {
                    vertxResponse.finish();
                } catch (IOException var10) {
                    var10.printStackTrace();
                }
            }

        });
    }
}
