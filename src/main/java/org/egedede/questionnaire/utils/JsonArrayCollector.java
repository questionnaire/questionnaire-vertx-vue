package org.egedede.questionnaire.utils;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

/**
 *
 */
public class JsonArrayCollector implements Collector<JsonObject, JsonArray, JsonArray> {
    @Override
    public Supplier<JsonArray> supplier() {
        return new Supplier<JsonArray>() {
            @Override
            public JsonArray get() {
                return new JsonArray();
            }
        };
    }

    @Override
    public BiConsumer<JsonArray, JsonObject> accumulator() {
        return (ref, newValue)-> ref.add(newValue);
    }

    @Override
    public BinaryOperator<JsonArray> combiner() {
        return (left, right) -> {
            left.addAll(right);
            return left;
        };
    }

    @Override
    public Function<JsonArray, JsonArray> finisher() {
        return (x -> x);
    }

    @Override
    public Set<Characteristics> characteristics() {
        return new HashSet<>();
    }
}
