package org.egedede.questionnaire.model;

/**
 *
 */
public interface Question {

    String getId();
    String getWording();
}
