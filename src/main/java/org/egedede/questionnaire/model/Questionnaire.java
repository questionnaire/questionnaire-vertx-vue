package org.egedede.questionnaire.model;

import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.egedede.questionnaire.utils.JsonArrayCollector;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 */
public class Questionnaire implements JsonObjectSerializable<Questionnaire> {

    public Questionnaire(String id, String title) {
        this.id = id;
        this.title = title;
        this.questions = new ArrayList<>();
    }

    private String id;
    private String title;
    private List<Question> questions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    @Override
    public JsonObject toJson(Questionnaire object) {
        JsonArray questionsArray = questions.stream()
                .map(question -> toJson(question))
                .collect(new JsonArrayCollector());
        JsonObject result = new JsonObject()
                .put("id",id)
                .put("title",title)
                .put("questions", questionsArray)
                ;
        return result;
    }

    private JsonObject toJson(Question question) {
        switch (question.getClass().getName()){
            case "FreeQuestion":
                return ((FreeQuestion)question).toJson((FreeQuestion) question);
        }
        throw new IllegalArgumentException("bouh");
    }

    @Override
    public Questionnaire fromJson(JsonObject json) {
        Questionnaire result = new Questionnaire(json.getString("id"), json.getString("title"));
        JsonArray questions = json.getJsonArray("questions");
        for(int i=0;i<questions.size();i++){
            result.getQuestions().add(new FreeQuestion("","",1).fromJson(questions.getJsonObject(i)));
        }
        return result;
    }
}
