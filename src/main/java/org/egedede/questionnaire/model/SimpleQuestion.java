package org.egedede.questionnaire.model;

import io.vertx.core.json.JsonObject;

/**
 *
 */
public class SimpleQuestion implements Question {

    private String id;
    private String wording;

    public SimpleQuestion(String id, String wording) {
        this.id = id;
        this.wording = wording;
    }

    @Override
    public String getWording() {
        return wording;
    }

    public void setWording(String wording) {
        this.wording = wording;
    }


    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public JsonObject toJson(SimpleQuestion object) {
        JsonObject result = new JsonObject()
                .put("wording", wording);
        return result;
    }

    public SimpleQuestion fromJson(JsonObject json) {
        return json.mapTo(SimpleQuestion.class);
    }
}
