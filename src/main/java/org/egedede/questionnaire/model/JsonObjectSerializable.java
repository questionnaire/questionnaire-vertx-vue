package org.egedede.questionnaire.model;

import io.vertx.core.json.JsonObject;

/**
 *
 */
public interface JsonObjectSerializable<T> {
    JsonObject toJson(T object);
    T fromJson(JsonObject json);
}
