package org.egedede.questionnaire.model;

import io.vertx.core.json.JsonObject;

/**
 *
 */
public class FreeQuestion extends SimpleQuestion implements JsonObjectSerializable<FreeQuestion> {


    private int nbLines;

    public FreeQuestion(String id, String wording, int nbLines) {
        super(id, wording);
        this.nbLines = nbLines;
    }

    public int getNbLines() {
        return nbLines;
    }

    public void setNbLines(int nbLines) {
        this.nbLines = nbLines;
    }

    @Override
    public JsonObject toJson(FreeQuestion object) {
        JsonObject result = super.toJson(object);
        result.put("nbLines", nbLines);
        return result;
    }

    @Override
    public FreeQuestion fromJson(JsonObject json) {
        return new FreeQuestion(json.getString("id"), json.getString("wording"),json.getInteger("nbLines"));
    }
}
