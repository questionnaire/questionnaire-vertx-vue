package org.egedede.questionnaire;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;
import org.egedede.questionnaire.utils.VRHandler;
import org.jboss.resteasy.plugins.server.vertx.VertxRequestHandler;
import org.jboss.resteasy.plugins.server.vertx.VertxResteasyDeployment;

/**
 *
 */
public class Server extends AbstractVerticle {
    @Override
    public void start() throws Exception {
        VertxResteasyDeployment deployment = new VertxResteasyDeployment();
        deployment.start();
        deployment.getRegistry().addPerInstanceResource(Controller.class);

        vertx.deployVerticle(Backend.class, new DeploymentOptions());
//        vertx.deployVerticle(StaticServer.class, new DeploymentOptions());
        // Start the front end server using the Jax-RS controller
        Router router = Router.router(vertx);
        router.route().handler(rq -> {
            System.out.println("Server.start routes 1 "+rq.request().method()+" "+rq.request().path());
            rq.next();
        });
        router.route("/static/*").handler(StaticHandler.create());
        router.route().handler(new VRHandler(vertx, deployment));
        vertx.createHttpServer()
//                .requestHandler(new VertxRequestHandler(vertx, deployment))
                .requestHandler(router::accept)
                .listen(8080, ar -> {
                    System.out.println("Server started on port "+ ar.result().actualPort());
                });
    }
}
