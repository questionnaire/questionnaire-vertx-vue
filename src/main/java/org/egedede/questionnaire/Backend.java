package org.egedede.questionnaire;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.egedede.questionnaire.export.pdf.PdfExporter;
import org.egedede.questionnaire.model.FreeQuestion;
import org.egedede.questionnaire.model.Questionnaire;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class Backend extends AbstractVerticle {

    private Map<String, JsonObject> questionnaires = new HashMap<>();
    @Override
    public void start() throws Exception {

        // A simple backend
        vertx.eventBus().<JsonObject>consumer("backend", msg -> {
            try {
                JsonObject json = msg.body();

                String op = json.getString("op", "");
                switch (op) {
                    case "add": {
                        createQuestionnaire(msg);
                        break;
                    }
                    case "get": {
                        getQuestionnaire(msg);
                        break;
                    }
                    case "post": {
                        String questionnaireID = json.getString("id");
                        JsonObject questionnaire = json.getJsonObject("questionnaire");
                        questionnaire.put("id", questionnaireID);
                        msg.reply(addQuestionnaire(questionnaire));
                        break;
                    }
                    case "list": {
                        getQuestionnaires(msg);
                        break;
                    }
                    case "addQuestion": {
                        addQuestion(msg);
                        break;
                    }
                    case "synchronize": {
                        synchronize(msg);
                        break;
                    }
                    case "export": {
                        export(msg);
                        break;
                    }
                    default: {
                        System.err.println("Backend.start "+op+" is an unknown operation");
                        msg.fail(0, "operation not permitted");
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
                msg.fail(500, "Issue : "+e.getMessage());
            }
        });

    }

    private void synchronize(Message<JsonObject> msg) {
        JsonObject json = msg.body();
        // no selected here
        JsonArray response = new JsonArray();
        JsonArray questionnairess = json.getJsonArray("questionnaires");
        for(Object object : questionnairess){
            JsonObject item = (JsonObject) object;
            String id = item.getString("id");
            if(questionnaires.get(id)==null){
                System.out.println("Backend.synchronize "+id+" included");
                questionnaires.put(id, item);
            }
        }
        msg.reply(response);

    }

    private void addQuestion(Message<JsonObject> msg) {
        JsonObject json = msg.body();
        String questionnaireID = json.getString("id");
        JsonObject jsonQuestionnaire = questionnaires.get(questionnaireID);
        Questionnaire questionnaire = new Questionnaire("plop", "plop").fromJson(jsonQuestionnaire);
        Object nbLinesData = json.getValue("nbLines");
        int nbLines;
        if(nbLinesData instanceof Number){
            nbLines = ((Number)nbLinesData).intValue();
        } else {
            nbLines = Integer.parseInt(nbLinesData.toString());
        }
        FreeQuestion question = new FreeQuestion(
                UUID.randomUUID().toString(),
                json.getString("wording"),
                nbLines);
        questionnaire.getQuestions().add(question);
        msg.reply(question.toJson(question).encode());
    }

    private void getQuestionnaires(Message<JsonObject> msg) {
        System.out.println("In backen, about to read questionnaires");
        JsonArray arr = new JsonArray();
        questionnaires.forEach((k, v) -> arr.add(v));
        msg.reply(arr);
        return;
    }

    private void getQuestionnaire(Message<JsonObject> msg) {
        JsonObject json = msg.body();
        String questionnaireID = json.getString("id");
        msg.reply(questionnaires.get(questionnaireID));
    }

    private void createQuestionnaire(Message<JsonObject> msg) {
        JsonObject json = msg.body();
        String questionnaireID = UUID.randomUUID().toString();
        Questionnaire questionnaire = new Questionnaire(questionnaireID, json.getString("title"));
        JsonObject value = questionnaire.toJson(questionnaire);
        questionnaires.put(questionnaireID, value);
        msg.reply(value.encode());
    }


    private void export(Message<JsonObject> msg){
        JsonObject json = msg.body();
        System.err.println("Backend.export "+msg.body());
        String questionnaireID = UUID.randomUUID().toString();
        Questionnaire questionnaire = new Questionnaire(questionnaireID, json.getString("title"));
        try{
            PdfExporter exporter = new PdfExporter();
            byte[] toSave = exporter.export(questionnaire);
            msg.reply(Base64.getEncoder().encodeToString(toSave));
        } catch(Exception e){
            e.printStackTrace();
            msg.fail(-1,e.getMessage());
        }
    }

    private String addQuestionnaire(JsonObject questionnaire) {
        Questionnaire questionnaire1 = new Questionnaire(
                UUID.randomUUID().toString(),"plop)").fromJson(questionnaire);
            questionnaires.put(questionnaire1.getId(), questionnaire);
            return questionnaire.encode();
    }


}

