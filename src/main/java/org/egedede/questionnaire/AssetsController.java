package org.egedede.questionnaire;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.streams.Pump;

import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import java.io.InputStream;

/**
 *
 */

public class AssetsController {

    @GET
    @Path("/assets/{name}")
    public InputStream asset(@Context Vertx vertx,
                             @PathParam("name") String name){
        AsyncFile buffer = vertx.fileSystem().openBlocking("assets/"+name,new OpenOptions());
        return null;
    }

}
