package org.egedede.questionnaire.export;

import org.egedede.questionnaire.export.pdf.PdfExporter;
import org.egedede.questionnaire.model.Questionnaire;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.UUID;

/**
 *
 */
public class PdfExporterTest {

    @Test
    public void testTest() throws IOException {
        Assertions.assertTrue(true);
        PdfExporter exporter = new PdfExporter();
        byte[] export = exporter.export(new Questionnaire("id", "Title"));
        Files.write(Paths.get("/tmp", UUID.randomUUID().toString()+".pdf")
        ,export, StandardOpenOption.CREATE_NEW);
    }
}
